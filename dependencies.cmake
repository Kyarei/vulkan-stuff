CMAKE_POLICY(SET CMP0072 NEW)

MACRO(SET_DEPENDENCIES target_name)

  FIND_PACKAGE(Vulkan)

  if (Vulkan_FOUND)
    TARGET_INCLUDE_DIRECTORIES(${target_name} SYSTEM PUBLIC "${Vulkan_INCLUDE_DIR}")
  else()
    MESSAGE(FATAL_ERROR "Could not find OpenGL")
  endif()

  if(NOT CMAKE_BUILD_TYPE)
      set(CMAKE_BUILD_TYPE Debug)
  endif(NOT CMAKE_BUILD_TYPE)

  SET_PROPERTY(TARGET ${target_name} PROPERTY CXX_STANDARD 20)
  SET_PROPERTY(TARGET ${target_name} PROPERTY CXX_EXTENSIONS FALSE)
  SET(CXX_FLAGS_COMMON "-Wall -Wpedantic -Wextra -Werror -Wzero-as-null-pointer-constant -Wdouble-promotion -Wfloat-equal")

  IF("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    SET(CXX_FLAGS_COMMON "${CXX_FLAGS_COMMON} -Wsuggest-override -Wduplicated-branches -Wduplicated-cond -Wshadow=local")
  ENDIF()

  FIND_PACKAGE(OpenMP COMPONENTS CXX REQUIRED)
  IF(OpenMP_CXX_FOUND)
    SET(CXX_FLAGS_COMMON "${CXX_FLAGS_COMMON} ${OpenMP_CXX_FLAGS}")
  ENDIF(OpenMP_CXX_FOUND)

  SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${CXX_FLAGS_COMMON} -Og -ggdb")
  SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${CXX_FLAGS_COMMON} -O3 -march=native")

  SET_PROPERTY(TARGET ${target_name} PROPERTY C_STANDARD 99)
  SET_PROPERTY(TARGET ${target_name} PROPERTY C_EXTENSIONS FALSE)
  SET(C_FLAGS_COMMON "-Wall -Wpedantic -Wextra -Werror")
  SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} ${C_FLAGS_COMMON} -Og -g")
  SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${C_FLAGS_COMMON} -O3")

  IF(USE_GPERFTOOLS_PROFILER)
    SET(PROFILER_LIBRARY "-lprofiler")
    MESSAGE(STATUS "Using gperftools profiler")
  ELSE(USE_GPERFTOOLS_PROFILER)
    SET(PROFILER_LIBRARY "")
  ENDIF(USE_GPERFTOOLS_PROFILER)

  ADD_COMPILE_DEFINITIONS(GLM_UNRESTRICTED_GENTYPE=1)

ENDMACRO()
